import psycopg2

con = psycopg2.connect("dbname=gamaexp user=gama password=gama")

cur = con.cursor()

cur.execute("INSERT INTO tmptableanalyse Select id_simulation,max(id_configuration) as id_configuration,avg(nervousness) as nervouness,(sum(locationX*nervousness))/sum(nervousness) as baryX,(sum(locationY*nervousness))/sum(nervousness) as baryY,count(*)/(count(distinct time)*1.0) as nPeople, sum(isNervous::int)/(count(distinct time)*1.0) as nbNervousPeople from"
"(select *,(tb1.nervousness>=0.5) AS isNervous FROM (select id_simulation,id_configuration,sim_id,step,activation_step,unnest(states[array_upper(states,1)][2:2]) as locationX,"
"            unnest(states[array_upper(states,1)][3:3]) as locationY,"
"            unnest(states[array_upper(states,1)][4:4]) as nervousness,"
"            unnest(states[array_upper(states,1)][5:5]) as directionx,"
"            unnest(states[array_upper(states,1)][6:6]) as directiony,"
"             (step-200+generate_series(1,array_upper(states,1))-1) AS time"
"            FROM state NATURAL JOIN simulation  WHERE states[1][1] = 1 AND NOT activation_step IS NULL"
" ) as tb1"
" WHERE time >= activation_step and time <= 4000) as tb2"
" group by id_simulation ;",)