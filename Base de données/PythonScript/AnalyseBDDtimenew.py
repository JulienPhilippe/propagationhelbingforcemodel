import psycopg2
from math import sqrt
import matplotlib.pyplot as plt

#con = psycopg2.connect("dbname=gamaexp user=litistech")
con =  psycopg2.connect("dbname=gamaexp user=gama password=gama")

cur = con.cursor()

simulation_Set_Id = 1

cur.execute("SELECT * FROM analyse_temporel_simulation(1)")

dict_donne = {}

rowSetSize = 20

for row in cur:
    if row[0] in dict_donne:
        dict_donne[row[0]][2].append(row[3:])
    else:
        dict_donne[row[0]] = (row[1],row[2],[row[3:]])

plt.figure(figsize=(12.75,7))

liste_lambda = ["lambda : 2","lambda : 3","lambda : 4","lambda : 5"]
for l in liste_lambda:
    dataString = "name,step,densite\n"
    plt.figure(figsize=(12.75, 7))
    for type in dict_donne:
        if str(dict_donne[type][1]).__contains__(l):
            legend = []
            axis = []

            i = 0;

            rowSet = []
            while i < rowSetSize/2:
                rowSet.append( dict_donne[type][2][0])
                i = i + 1
            while i < rowSetSize:
                rowSet.append(dict_donne[type][2][i-(int(rowSetSize/2))])
                i = i + 1

            i = 0
            while i < len(dict_donne[type][2]):
                mean = 0
                cpt = 0
                while cpt < len(rowSet):
                    mean = mean + rowSet[cpt][2]
                    cpt = cpt + 1
                mean = mean/len(rowSet)

                row = dict_donne[type][2][i]
                legend.append(row[0])
                axis.append(mean)
                rowSet.pop(0)
                if i <  len(dict_donne[type][2])-5:
                    rowSet.append( dict_donne[type][2][i+5])
                i = i + 1

            #for row in dict_donne[type][2]:
            #    legend.append(row[0])
            #    axis.append(row[2])

            for i in range(len(legend)):
               dataString += str(dict_donne[type][0]) + "," + str(legend[i]) + "," + str(axis[i]) + "\n"


            plt.plot(legend, axis, label=dict_donne[type][0])

    f = open('/home/julienlitis/Images/test' + str(simulation_Set_Id) + '/' + "testDensité2" + l + '.csv', "w")
    # f = open('/home/litistech/Images/' + str(simulation_Set_Id) + '/' + "testDensité2" + l + '.csv', "w")
    f.write(dataString)
    f.close()
    plt.title("Densité au cours du temps pour " + l ,fontsize=26)
    plt.xlabel("Temps",fontsize=17)
    plt.ylabel("Densité",fontsize=17)
    plt.legend()
    axes = plt.gca()
    axes.xaxis.set_tick_params(labelsize = 14)
    axes.yaxis.set_tick_params(labelsize = 14)
    plt.savefig('/home/julienlitis/Images/test' + str(simulation_Set_Id) + '/' + "testDensité2" + l + '.png', format='png', bbox_inches="tight", dpi=400, pad_inches=0.1)
    #plt.savefig('/home/litistech/Images/' + str(simulation_Set_Id) + '/' + "testDensité2"+ l +'.png', format='png',bbox_inches="tight", dpi=400, pad_inches=0.1)

plt.figure(figsize=(12.75,7))

liste_lambda = ["lambda : 2","lambda : 3","lambda : 4","lambda : 5"]
for l in liste_lambda:
    dataString = "name,step,nervosite\n"
    plt.figure(figsize=(12.75, 7))
    for type in dict_donne:
        if str(dict_donne[type][1]).__contains__(l):
            legend = []
            axis = []

            i = 0;

            rowSet = []
            while i < rowSetSize / 2:
                rowSet.append(dict_donne[type][2][0])
                i = i + 1
            while i < rowSetSize:
                rowSet.append(dict_donne[type][2][i - (int(rowSetSize / 2))])
                i = i + 1

            i = 0
            while i < len(dict_donne[type][2]):
                mean = 0
                cpt = 0
                while cpt < len(rowSet):
                    mean = mean + rowSet[cpt][4]
                    cpt = cpt + 1
                mean = mean / len(rowSet)

                row = dict_donne[type][2][i]
                legend.append(row[0])
                axis.append(mean)
                rowSet.pop(0)
                if i < len(dict_donne[type][2]) - 5:
                    rowSet.append(dict_donne[type][2][i + 5])
                i = i + 1

            #for row in dict_donne[type][2]:
            #    legend.append(row[0])
            #    axis.append(row[4])


            for i in range(len(legend)):
                dataString += str(dict_donne[type][0]) + "," + str(legend[i]) + "," + str(axis[i]) + "\n"



            plt.plot(legend, axis, label=dict_donne[type][0])

    f = open('/home/julienlitis/Images/test' + str(simulation_Set_Id) + '/' + "testNervosite2" + l + '.csv', "w")
    # f = open('/home/litistech/Images/' + str(simulation_Set_Id) + '/' + "testNervosite2" + l + '.csv', "w")
    f.write(dataString)
    f.close()
    plt.title("Nervosité au cours du temps pour " + l ,fontsize=26)
    plt.xlabel("Temps",fontsize=17)
    plt.ylabel("Nervosité",fontsize=17)
    plt.legend()
    axes = plt.gca()
    axes.xaxis.set_tick_params(labelsize = 14)
    axes.yaxis.set_tick_params(labelsize = 14)
    plt.savefig('/home/julienlitis/Images/test' + str(simulation_Set_Id) + '/' + "testNervosité2" + l.replace(" ", "") + '.png', format='png',bbox_inches="tight", dpi=400, pad_inches=0.1)
    #plt.savefig('/home/litistech/Images/' + str(simulation_Set_Id) + '/' + "testNervosité2"+ l.replace(" ","") +'.png', format='png',bbox_inches="tight", dpi=400, pad_inches=0.1)

liste_lambda = ["lambda : 2","lambda : 3","lambda : 4","lambda : 5"]
for l in liste_lambda:
    dataString = "name,step,nervosite/densite\n"
    plt.figure(figsize=(12.75, 7))
    for type in dict_donne:
        if str(dict_donne[type][1]).__contains__(l):
            legend = []
            axis = []
            for row in dict_donne[type][2]:
                legend.append(row[2])
                axis.append(row[4])



            for i in range(len(legend)):
                dataString += str(dict_donne[type][0]) + "," + str(legend[i]) + "," + str(axis[i]) + "\n"



            plt.plot(legend, axis, label=dict_donne[type][0])

    f = open('/home/julienlitis/Images/test' + str(simulation_Set_Id) + '/' + "testND2" + l + '.csv',
             # f = open('/home/litistech/Images/' + str(simulation_Set_Id) + '/' + "testND2" + l + '.csv',
             "w")
    f.write(dataString)
    f.close()
    plt.title("Nervosité en fonction de la densité pour " + l ,fontsize=26)
    plt.xlabel("Densité",fontsize=17)
    plt.ylabel("Nervosité",fontsize=17)
    plt.legend()
    axes = plt.gca()
    axes.xaxis.set_tick_params(labelsize = 14)
    axes.yaxis.set_tick_params(labelsize = 14)

    plt.savefig('/home/julienlitis/Images/test' + str(simulation_Set_Id) + '/' + "testND2" + l + '.png', format='png', bbox_inches="tight", dpi=400,pad_inches=0.1)
    #plt.savefig('/home/litistech/Images/' + str(simulation_Set_Id) + '/' + "testND"+ l +'.png', format='png',bbox_inches="tight", dpi=400, pad_inches=0.1)


    