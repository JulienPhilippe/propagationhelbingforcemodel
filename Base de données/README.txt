To initiate DATABASE for simulation

	1- Launch CREATETABLEexp.sql to create all the table we need

	2- Launch CREATEVIEWexp.sql to create view_tmptableanalyseaverage who give average value of nervousness,density.. for each simulation

	3- Lauch CREATEFUNCTIONexp.sql to create all the function useful to analyse the data (needed to the GAMA sim)

To initiate config for a set of simulation

	INSERTCONFIGexp.sql				--> Set experiments with No propagation, propagation by maximum value and propagation with average value. No threshold, epsilon = [ 0.1, 0.5, 0.9 ] Lambda = [2 , 3 ,4 , 5]  
	INSERTCONFIGexp2Threshold.sql	--> Set experiment to test propagation with a threshold mecanism
	INSERTCONFIGexpEpsilon.sql		--> Set experiment to test new value of epsilon [ 0.6 , 0.7, 0.8 ] to seek the point of shifting

	For the corridor situation, you have to execute INSERTCONFIGexp.sql because it also settle obstacle and agent spawn.
	Every of this files setup relation between parameterset, agent spawn and obstacle
