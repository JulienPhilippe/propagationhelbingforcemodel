import psycopg2
from math import sqrt
import matplotlib.pyplot as plt

#con = psycopg2.connect("dbname=gamaexp user=litistech")
con =  psycopg2.connect("dbname=gamaexp user=gama password=gama")


cur = con.cursor()

simulations_set = 1

cur.execute("SELECT * FROM view_tmptableanalyseaverage where id_simulationset=1")

dict_donne = {}


for row in cur:
    if row[9] in dict_donne:
        dict_donne[row[9]].append((row[10],row[2:8]))
    else:
        dict_donne[row[9]] = [(row[10],row[2:8])] #(row[9],row[10],[row[2:8]])

dataString = "name,poisson_lambda, nervousness,nbpeople,densite,nervouspeople,baryx,baryy,distancebarydoor\n"

for k in dict_donne:
    for dataSet in dict_donne[k]:
        dataString += str(k + "," + dataSet[0])
        for elem in dataSet[1]:
            dataString += "," + str(elem)
        dataString += "\n"

print(dataString)
f = open('/home/litistech/Images/' + str(simulations_set) + '/AnalyseBDDavg.csv', "w")
f.write(dataString)
f.close()

plt.figure(figsize=(12.75,7))

#Nervousness
for k in dict_donne:
    axis = []
    legend = []
    for dataSet in dict_donne[k]:
        legend.append(dataSet[0])
        axis.append(dataSet[1][0])
    plt.plot(legend, axis, label=k)

plt.title("Nervosité moyenne" ,fontsize=26)
plt.xlabel("Lambda",fontsize=17)
plt.ylabel("Nervosité",fontsize=17)
plt.legend()
axes = plt.gca()
axes.xaxis.set_tick_params(labelsize = 14)
axes.yaxis.set_tick_params(labelsize = 14)

plt.savefig('/home/julienlitis/Images/testPy/' + str(simulations_set) + '/testNervouss' + str(simulations_set) + '.png', format='png',bbox_inches="tight", dpi=400, pad_inches=0.1)

plt.figure(figsize=(12.75,7))

#NbPeople
for k in dict_donne:
    axis = []
    legend = []
    for dataSet in dict_donne[k]:
        legend.append(dataSet[0])
        axis.append(dataSet[1][1])
    plt.plot(legend, axis, label=k)

plt.title("Nb People" ,fontsize=26)
plt.xlabel("Lambda",fontsize=17)
plt.ylabel("NbPeople",fontsize=17)
plt.legend()
axes = plt.gca()
axes.xaxis.set_tick_params(labelsize = 14)
axes.yaxis.set_tick_params(labelsize = 14)

plt.savefig('/home/julienlitis/Images/testPy/' + str(simulations_set) + '/testNbPeople' + str(simulations_set) + '.png', format='png',bbox_inches="tight", dpi=400, pad_inches=0.1)

plt.figure(figsize=(12.75,7))

#Densité
for k in dict_donne:
    axis = []
    legend = []
    for dataSet in dict_donne[k]:
        legend.append(dataSet[0])
        axis.append(dataSet[1][2])
    plt.plot(legend, axis, label=k)

plt.title("Densite" ,fontsize=26)
plt.xlabel("Lambda",fontsize=17)
plt.ylabel("People by m2",fontsize=17)
plt.legend()
axes = plt.gca()
axes.xaxis.set_tick_params(labelsize = 14)
axes.yaxis.set_tick_params(labelsize = 14)

plt.savefig('/home/julienlitis/Images/testPy/' + str(simulations_set) + '/testDensite' + str(simulations_set) + '.png', format='png',bbox_inches="tight", dpi=400, pad_inches=0.1)





