--MAXIMUM--
INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Maximum',FALSE,40,2,TRUE,0.6,60,7,2000,0.08,0.12,120000,240000,TRUE,0.0);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Maximum',FALSE,40,2,TRUE,0.7,60,7,2000,0.08,0.12,120000,240000,TRUE,0.0);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Maximum',FALSE,40,2,TRUE,0.8,60,7,2000,0.08,0.12,120000,240000,TRUE,0.0);



--AVERAGE--
INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Mean',FALSE,40,2,TRUE,0.6,60,7,2000,0.08,0.12,120000,240000,TRUE,0.0);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Mean',FALSE,40,2,TRUE,0.7,60,7,2000,0.08,0.12,120000,240000,TRUE,0.0);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Mean',FALSE,40,2,TRUE,0.8,60,7,2000,0.08,0.12,120000,240000,TRUE,0.0);


--CONFIGURATION
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Maximum interaction','lambda : 3','Bottleneck',49);

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Maximum interaction','lambda : 3','Bottleneck',50);

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Maximum interaction','lambda : 3','Bottleneck',51);


INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Average interaction','lambda : 3','Bottleneck',52);  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Average interaction','lambda : 3','Bottleneck',53);  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Average interaction','lambda : 3','Bottleneck',54);  


INSERT INTO includeobstacle (id_obstacle,id_configuration)
(SELECT id_obstacle,id_configuration FROM obstacle,configuration where id_configuration >= 79);

INSERT INTO includeagent (id_configuration,id_agentSet,id_area,listOrder)
(SELECT id_configuration, id_agentset,id_area,id_area FROM configuration,agentset, area WHERE id_agentset = 2 AND (id_configuration >= 79));
