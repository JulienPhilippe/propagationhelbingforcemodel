import psycopg2
from math import sqrt
import matplotlib.pyplot as plt

con = psycopg2.connect("dbname=gamaexp user=litistech")

cur = con.cursor()

idConfigurationDepart = 34

while idConfigurationDepart < 79:
        cur.execute("select p.threshold, v.* from view_tmptableanalyseaverage v inner join configuration c on v.id_configuration = c.id_configuration inner join parameterset p on c.id_parameterset = p.id_parameterset where id_simulationset=4 and v.id_configuration >= " + str(idConfigurationDepart) + " AND v.id_configuration <=" + str(idConfigurationDepart+14))

        dict_donne = {}

        for row in cur:
            name = str(row[10]).split(" ")[0] + " " + str(row[10]).split(" ")[1]

            if name in dict_donne:
                dict_donne[name].append(row[0:11])
            else:
                dict_donne[name] = [row[0:11]]  # (row[9],row[10],[row[2:8]])

        plt.figure(figsize=(12.75, 7))
        print(dict_donne)
        # Nervousness
        for k in dict_donne:
            axis = []
            legend = []
            for dataSet in dict_donne[k]:
                legend.append(dataSet[0])
                axis.append(dataSet[5])
            plt.plot(legend, axis, label=k)

        plt.title("Densité moyenne", fontsize=26)
        plt.xlabel("Seuil", fontsize=17)
        plt.ylabel("Densité", fontsize=17)
        plt.legend()
        axes = plt.gca()
        axes.xaxis.set_tick_params(labelsize=14)
        axes.yaxis.set_tick_params(labelsize=14)

        plt.savefig('/home/litistech/Images/thresholdNervouss/thresholdDensite ' + str(idConfigurationDepart) + '_' + str(idConfigurationDepart+14) + '.png', format='png', bbox_inches="tight", dpi=400, pad_inches=0.1)

        idConfigurationDepart = idConfigurationDepart + 15

