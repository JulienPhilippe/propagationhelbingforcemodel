------------------------------------------------------------
--        Script Postgre 
------------------------------------------------------------

DROP TABLE interactions,agent, agentset,area, circularobstacle, configuration, includeagent,obstacle,parameterset,partofaset,simulation,simulationset,wall,includeobstacle,state CASCADE ;


------------------------------------------------------------
-- Table: Obstacle
------------------------------------------------------------
CREATE TABLE public.Obstacle(
	id_obstacle   SERIAL NOT NULL ,
	coordX        FLOAT  NOT NULL ,
	coordY        FLOAT  NOT NULL  ,
	insert_date	TIMESTAMP,
	CONSTRAINT Obstacle_PK PRIMARY KEY (id_obstacle)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Wall
------------------------------------------------------------
CREATE TABLE public.Wall(
	id_obstacle   INT  NOT NULL ,
	id_wall       SERIAL  NOT NULL ,
	largeur       FLOAT  NOT NULL ,
	longueur      FLOAT  NOT NULL ,
	insert_date	TIMESTAMP,
	CONSTRAINT Wall_PK PRIMARY KEY (id_obstacle,id_wall)

	,CONSTRAINT Wall_Obstacle_FK FOREIGN KEY (id_obstacle) REFERENCES public.Obstacle(id_obstacle)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: CircularObstacle
------------------------------------------------------------
CREATE TABLE public.CircularObstacle(
	id_obstacle           INT  NOT NULL ,
	id_CircularObstacle   INT  NOT NULL ,
	rayon                 FLOAT  NOT NULL ,
	coordX                FLOAT  NOT NULL ,
	coordY                FLOAT  NOT NULL  ,
	insert_date	TIMESTAMP,
	CONSTRAINT CircularObstacle_PK PRIMARY KEY (id_obstacle,id_CircularObstacle)

	,CONSTRAINT CircularObstacle_Obstacle_FK FOREIGN KEY (id_obstacle) REFERENCES public.Obstacle(id_obstacle)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Area
------------------------------------------------------------
CREATE TABLE public.Area(
	id_area    SERIAL NOT NULL ,
	coordLTx   FLOAT  NOT NULL ,
	coordLTy   FLOAT  NOT NULL ,
	coordRDx   FLOAT  NOT NULL ,
	coordRDy   FLOAT  NOT NULL  ,
	insert_date	TIMESTAMP,
	CONSTRAINT Area_PK PRIMARY KEY (id_area)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: AgentSet
------------------------------------------------------------
CREATE TABLE public.AgentSet(
	id_agentSet       SERIAL NOT NULL ,
	paramGeneration   FLOAT  NOT NULL ,
	isPoisson         BOOL  NOT NULL ,
	color             VARCHAR (25)   ,
	insert_date	TIMESTAMP,
	CONSTRAINT AgentSet_PK PRIMARY KEY (id_agentSet)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Node
------------------------------------------------------------
--CREATE TABLE public.Node(
--	id_node              SERIAL NOT NULL ,
--	agentNumero          INT  NOT NULL ,
--	coordX               FLOAT  NOT NULL ,
--	coordY               FLOAT  NOT NULL ,
--	innerNerv            FLOAT  NOT NULL ,
--	lastNervousness      FLOAT  NOT NULL ,
--	currentNervousness   FLOAT  NOT NULL ,
--	cycle                INT  NOT NULL  ,
--	CONSTRAINT Node_PK PRIMARY KEY (id_node)
--)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Edge
------------------------------------------------------------
--CREATE TABLE public.Edge(
--	id_edge              SERIAL NOT NULL ,
--	cycle                INT  NOT NULL ,
--	nervousnessPassing   FLOAT  NOT NULL ,
--	id_node              INT  NOT NULL ,
--	id_node_Arrivee      INT  NOT NULL  ,
--	CONSTRAINT Edge_PK PRIMARY KEY (id_edge)

--	,CONSTRAINT Edge_Node_FK FOREIGN KEY (id_node) REFERENCES public.Node(id_node)
--	,CONSTRAINT Edge_Node0_FK FOREIGN KEY (id_node_Arrivee) REFERENCES public.Node(id_node)
--)WITHOUT OIDS;


------------------------------------------------------------
-- Table: SimulationSet
------------------------------------------------------------
CREATE TABLE public.SimulationSet(
	id_simulationSet   SERIAL NOT NULL ,
	name               VARCHAR (25)  ,
	Commentary         VARCHAR (2000)    ,
	insert_date	TIMESTAMP,
	CONSTRAINT SimulationSet_PK PRIMARY KEY (id_simulationSet)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: ParameterSet
------------------------------------------------------------
CREATE TABLE public.ParameterSet(
	id_parameterSet               SERIAL NOT NULL ,
	fluctuationType               VARCHAR (25) NOT NULL ,
	deltaT                        FLOAT  NOT NULL ,
	relaxationTime                FLOAT  NOT NULL ,
	isRespawn                     BOOL  NOT NULL ,
	isFluctuation                 BOOL  NOT NULL ,
	pedestrianSpeed               FLOAT  NOT NULL ,
	pedestrianMaxSpeed            FLOAT  NOT NULL ,
	pedestrianMaxSize             FLOAT  NOT NULL ,
	pedestrianMinSize             FLOAT  NOT NULL ,
	simulationDuration            INT  NOT NULL ,
	temporalFieldIntervalLength   INT  NOT NULL ,
	interactionType               VARCHAR (25) NOT NULL ,
	is360                         BOOL  NOT NULL ,
	interactionAngle              FLOAT  NOT NULL ,
	interactionRange              FLOAT  NOT NULL ,
	isNervousnessTransmition      BOOL  NOT NULL ,
	interactionParameter          FLOAT  NOT NULL ,
	passingZoneIndex              INT  ,
	spaceLength                   FLOAT  NOT NULL ,
	spaceWidth                    FLOAT  NOT NULL ,
	socialForceStrength           FLOAT  NOT NULL ,
	socialForceThreshold          FLOAT  NOT NULL ,
	socialForceVision             FLOAT  NOT NULL ,
	bodyContactStrength           FLOAT  NOT NULL ,
	bodyFrictionStrength          FLOAT  NOT NULL  ,
	isNervousness                 BOOL  NOT NULL  ,
	threshold		 	FLOAT DEFAULT 0.0, 
	insert_date	TIMESTAMP,
	CONSTRAINT ParameterSet_PK PRIMARY KEY (id_parameterSet)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Configuration
------------------------------------------------------------
CREATE TABLE public.Configuration(
	id_configuration   SERIAL NOT NULL ,
	name               VARCHAR (25)  ,
	commentaire        VARCHAR (2000)   ,
	typeConfig         VARCHAR (25) NOT NULL ,
	id_parameterSet    INT  NOT NULL  ,
	insert_date	TIMESTAMP,
	CONSTRAINT Configuration_PK PRIMARY KEY (id_configuration)

	,CONSTRAINT Configuration_ParameterSet_FK FOREIGN KEY (id_parameterSet) REFERENCES public.ParameterSet(id_parameterSet)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Simulation
------------------------------------------------------------
CREATE TABLE public.Simulation(
	id_simulation      SERIAL NOT NULL ,
	date_depart        TIMESTAMP  NOT NULL ,
	date_fin           TIMESTAMP   ,
	activation_step	   INT,
	id_configuration   INT  NOT NULL  ,
	isover		   BOOLEAN,
	insert_date	TIMESTAMP,
	CONSTRAINT Simulation_PK PRIMARY KEY (id_simulation)

	,CONSTRAINT Simulation_Configuration_FK FOREIGN KEY (id_configuration) REFERENCES public.Configuration(id_configuration)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Agent
------------------------------------------------------------
CREATE TABLE public.Agent(
	id_agent        SERIAL NOT NULL ,
	sim_id		INTEGER NOT NULL, --Agent's id in the gama simulation ("int(self)")
	size            FLOAT  NOT NULL ,
	coorSpawnAX     FLOAT  NOT NULL ,
	coorSpawnAY     FLOAT  NOT NULL ,
	coorSpawnBX     FLOAT  NOT NULL ,
	coorSpawnBY     FLOAT  NOT NULL ,
	spawnTime       INT  NOT NULL ,
	id_simulation   INT  NOT NULL  ,
	insert_date	TIMESTAMP,
	CONSTRAINT Agent_PK PRIMARY KEY (id_agent)

	,CONSTRAINT Agent_Simulation_FK FOREIGN KEY (id_simulation) REFERENCES public.Simulation(id_simulation)
)WITHOUT OIDS;

------------------------------------------------------------
-- Table: Interactions
------------------------------------------------------------
--CREATE TABLE public.Interactions(
--	id_interaction		SERIAL NOT NULL,
--	id_simulation		INT NOT NULL,
--	cycle			INT NOT NULL,
--	sim_id_agent_seeing	INT NOT NULL,
--	sim_id_agent_seen	INT NOT NULL,
--	insert_date	TIMESTAMP,
--	CONSTRAINT Interactions_PK PRIMARY KEY (id_interaction)
--
--	,CONSTRAINT Interactions_Simulation_FK FOREIGN KEY (id_simulation) REFERENCES public.Simulation(id_simulation)
--)WITHOUT OIDS;

------------------------------------------------------------
-- Table: State
------------------------------------------------------------
/*CREATE TABLE public.State(
	id_state      SERIAL NOT NULL ,
	step          INT  NOT NULL ,
	coorX         FLOAT  NOT NULL ,
	coodY         FLOAT  NOT NULL ,
	nervousness   FLOAT  NOT NULL ,
	id_agent      INT  NOT NULL  ,
	CONSTRAINT State_PK PRIMARY KEY (id_state)

	,CONSTRAINT State_Agent_FK FOREIGN KEY (id_agent) REFERENCES public.Agent(id_agent)
)WITHOUT OIDS;
*/

------------------------------------------------------------
-- Table: State
------------------------------------------------------------
CREATE TABLE public.State(
	id_state      SERIAL NOT NULL ,
	step          INT  NOT NULL ,
	sim_id      INT  NOT NULL  ,
	id_simulation		INT NOT NULL,
	states		FLOAT[][] NOT NULL,
	insert_date	TIMESTAMP,
	CONSTRAINT State_PK PRIMARY KEY (id_state)

	,CONSTRAINT Agent_Simulation_FK FOREIGN KEY (id_simulation) REFERENCES public.Simulation(id_simulation)
)WITHOUT OIDS;

------------------------------------------------------------
-- Table: PartOfASet
------------------------------------------------------------
CREATE TABLE public.PartOfASet(
	id_simulationSet   INT  NOT NULL ,
	id_simulation      INT  NOT NULL  ,
	insert_date	TIMESTAMP,
	CONSTRAINT PartOfASet_PK PRIMARY KEY (id_simulationSet,id_simulation)

	,CONSTRAINT PartOfASet_SimulationSet_FK FOREIGN KEY (id_simulationSet) REFERENCES public.SimulationSet(id_simulationSet)
	,CONSTRAINT PartOfASet_Simulation0_FK FOREIGN KEY (id_simulation) REFERENCES public.Simulation(id_simulation)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: IncludeAgent
------------------------------------------------------------
CREATE TABLE public.IncludeAgent(
	id_configuration   INT  NOT NULL ,
	id_agentSet        INT  NOT NULL ,
	id_area            INT  NOT NULL ,
	listOrder          INT NOT NULL  ,
	insert_date	TIMESTAMP,
	CONSTRAINT IncludeAgent_PK PRIMARY KEY (id_configuration,id_agentSet,id_area)

	,CONSTRAINT IncludeAgent_Configuration_FK FOREIGN KEY (id_configuration) REFERENCES public.Configuration(id_configuration)
	,CONSTRAINT IncludeAgent_AgentSet0_FK FOREIGN KEY (id_agentSet) REFERENCES public.AgentSet(id_agentSet)
	,CONSTRAINT IncludeAgent_Area1_FK FOREIGN KEY (id_area) REFERENCES public.Area(id_area)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: IncludeObstacle
------------------------------------------------------------
CREATE TABLE public.IncludeObstacle(
	id_obstacle        INT  NOT NULL ,
	id_configuration   INT  NOT NULL  ,
	insert_date	TIMESTAMP,
	CONSTRAINT IncludeObstacle_PK PRIMARY KEY (id_obstacle,id_configuration)

	,CONSTRAINT IncludeObstacle_Obstacle_FK FOREIGN KEY (id_obstacle) REFERENCES public.Obstacle(id_obstacle)
	,CONSTRAINT IncludeObstacle_Configuration0_FK FOREIGN KEY (id_configuration) REFERENCES public.Configuration(id_configuration)
)WITHOUT OIDS;

------------------------------------------------------------
-- Table: tmpTableAnalyse
------------------------------------------------------------
CREATE TABLE public.tmpTableAnalyse(
	id_simulation		INT NOT NULL,
	id_configuration	INT NOT NULL,
	nervousness		FLOAT NOT NULL,
	baryX			FLOAT NOT NULL,
	baryY			FLOAT NOT NULL,
	distancebarydoor	FLOAT NOT NULL,
	nbPeople		FLOAT NOT NULL,
	densite			FLOAT NOT NULL,
	nbNervousPeople		FLOAT NOT NULL,
	insert_date	TIMESTAMP,
	CONSTRAINT tmpTableAnalyse_Configuation_FK FOREIGN KEY (id_configuration) REFERENCES public.Configuration(id_configuration),
	CONSTRAINT tmpTableAnalyse_Simulation_FK FOREIGN KEY (id_simulation) REFERENCES public.Simulation(id_simulation)
)WITHOUT OIDS;

COMMENT ON COLUMN public.ParameterSet.fluctuationType IS 'enum';
COMMENT ON COLUMN public.ParameterSet.interactionType IS 'enum';
COMMENT ON COLUMN public.Configuration.typeConfig IS 'enum';

GRANT SELECT, UPDATE, INSERT, REFERENCES ON ALL TABLES IN SCHEMA public TO gama;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO gama;



