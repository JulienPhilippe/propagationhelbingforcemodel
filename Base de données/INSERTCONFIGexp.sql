-------------SIMULATION PARAMETER-------------- 
---INSERT INTO parameterset (fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
--VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,CHANGE interactionType,FALSE,40,2,CHANGE isNervousnessTransmition,CHANGE interactionParameter,60,7,2000,0.08,0.12,120000,240000, CHANGE isNervousness);

--MAXIMUM--
INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Maximum',FALSE,40,2,TRUE,0.1,60,7,2000,0.08,0.12,120000,240000,TRUE,0.0);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Maximum',FALSE,40,2,TRUE,0.5,60,7,2000,0.08,0.12,120000,240000,TRUE,0.0);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Maximum',FALSE,40,2,TRUE,0.9,60,7,2000,0.08,0.12,120000,240000,TRUE,0.0);


--AVERAGE--
INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Mean',FALSE,40,2,TRUE,0.1,60,7,2000,0.08,0.12,120000,240000,TRUE,0.0);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Mean',FALSE,40,2,TRUE,0.5,60,7,2000,0.08,0.12,120000,240000,TRUE,0.0);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Mean',FALSE,40,2,TRUE,0.9,60,7,2000,0.08,0.12,120000,240000,TRUE,0.0);

--WITHOUT--
INSERT INTO parameterset (fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Without',FALSE,40,2,FALSE,0,60,7,2000,0.08,0.12,120000,240000, TRUE,0.0);

--NONERV--
INSERT INTO parameterset (fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Without',FALSE,40,2,FALSE,0,60,7,2000,0.08,0.12,120000,240000, FALSE,0.0);

-------------END SIMULATION PARAMETER----------

-------------AGENT----------------------
INSERT INTO area (coordLTx,coordLTy,coordRDx,coordRDy)
VALUES (-20,1.5,0,5.5);

INSERT INTO area (coordLTx,coordLTy,coordRDx,coordRDy)
VALUES (33,1.0,33.5,6.0);

INSERT INTO area (coordLTx,coordLTy,coordRDx,coordRDy)
VALUES (34.5,3.1,35,3.9);

INSERT INTO area (coordLTx,coordLTy,coordRDx,coordRDy)
VALUES (35.5,3.1,36,3.9);

INSERT INTO area (coordLTx,coordLTy,coordRDx,coordRDy)
VALUES (61,1.0,62,6.0);

INSERT INTO agentset (paramGeneration,isPoisson,color)
VALUES (2,TRUE,'black');

INSERT INTO agentset (paramGeneration,isPoisson,color)
VALUES (3,TRUE,'black');

INSERT INTO agentset (paramGeneration,isPoisson,color)
VALUES (4,TRUE,'black');

INSERT INTO agentset (paramGeneration,isPoisson,color)
VALUES (5,TRUE,'black');
-------------ENDAGENT----------------------

-------------------OBSTACLE-------------------
INSERT INTO obstacle (id_obstacle,coordX,coordY)
VALUES (0,40,0.5);

INSERT INTO obstacle (id_obstacle,coordX,coordY)
VALUES (1,40,6.5);

INSERT INTO obstacle (id_obstacle,coordX,coordY)
VALUES (2,35.5,2);

INSERT INTO obstacle (id_obstacle,coordX,coordY)
VALUES (3,35.5,5);

INSERT INTO wall (id_obstacle,id_wall,largeur,longueur)
VALUES (0,0,80,1);

INSERT INTO wall (id_obstacle,id_wall,largeur,longueur)
VALUES (1,1,80,1);

INSERT INTO wall (id_obstacle,id_wall,largeur,longueur)
VALUES (2,2,1,2);

INSERT INTO wall (id_obstacle,id_wall,largeur,longueur)
VALUES (3,3,1,2);
-------------------ENDOBSTACLE-------------------

update parameterset set passingZoneIndex = 3; -----------------TO DO A remplacer par un script


-------CONFIG----------
--MAXIMUM
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low Maximum interaction','lambda : 2','Bottleneck',1);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium Max interaction','lambda : 2','Bottleneck',2);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Maximum interaction','lambda : 2','Bottleneck',3);    

--AVERAGE
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low Average interaction','lambda : 2','Bottleneck',4);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium Av interaction','lambda : 2','Bottleneck',5);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Average interaction','lambda : 2','Bottleneck',6);    

--WITHOUT
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Without interaction','lambda : 2','Bottleneck',7);        

--NO NERV
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('No nervousness','lambda : 2','Bottleneck',8);    

--MAXIMUM
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low Maximum interaction','lambda : 3','Bottleneck',1);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium Max interaction','lambda : 3','Bottleneck',2);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Maximum interaction','lambda : 3','Bottleneck',3);    

--AVERAGE
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low Average interaction','lambda : 3','Bottleneck',4);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium Av interaction','lambda : 3','Bottleneck',5);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Average interaction','lambda : 3','Bottleneck',6);    

--WITHOUT
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Without interaction','lambda : 3','Bottleneck',7);        

--NO NERV
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('No nervousness','lambda : 3','Bottleneck',8);   

--MAXIMUM
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low Maximum interaction','lambda : 4','Bottleneck',1);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium Max interaction','lambda : 4','Bottleneck',2);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Maximum interaction','lambda : 4','Bottleneck',3);    

--AVERAGE
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low Average interaction','lambda : 4','Bottleneck',4);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium Av interaction','lambda : 4','Bottleneck',5);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Average interaction','lambda : 4','Bottleneck',6);    

--WITHOUT
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Without interaction','lambda : 4','Bottleneck',7);        

--NO NERV
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('No nervousness','lambda : 4','Bottleneck',8);

--MAXIMUM
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low Maximum interaction','lambda : 5','Bottleneck',1);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium Max interaction','lambda : 5','Bottleneck',2);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Maximum interaction','lambda : 5','Bottleneck',3);    

--AVERAGE
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low Average interaction','lambda : 5','Bottleneck',4);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium Av interaction','lambda : 5','Bottleneck',5);    

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High Average interaction','lambda : 5','Bottleneck',6);    

--WITHOUT
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Without interaction','lambda : 5','Bottleneck',7);        

--NO NERV
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('No nervousness','lambda : 5','Bottleneck',8);    
----------END CONFIG---------------

----------INCLUDE OBSTACLE

INSERT INTO includeobstacle (id_obstacle,id_configuration)
(SELECT id_obstacle,id_configuration FROM obstacle,configuration);


INSERT INTO includeagent (id_configuration,id_agentSet,id_area,listOrder)
(SELECT id_configuration, id_agentset,id_area,id_area FROM configuration,agentset, area WHERE id_agentset = 1 AND (id_configuration <= 8));

INSERT INTO includeagent (id_configuration,id_agentSet,id_area,listOrder)
(SELECT id_configuration, id_agentset,id_area,id_area FROM configuration,agentset, area WHERE id_agentset = 2 AND (id_configuration BETWEEN 9 AND 16));

INSERT INTO includeagent (id_configuration,id_agentSet,id_area,listOrder)
(SELECT id_configuration, id_agentset,id_area,id_area FROM configuration,agentset, area WHERE id_agentset = 3 AND (id_configuration BETWEEN 17 AND 24));

INSERT INTO includeagent (id_configuration,id_agentSet,id_area,listOrder)
(SELECT id_configuration, id_agentset,id_area,id_area FROM configuration,agentset, area WHERE id_agentset = 4 AND (id_configuration BETWEEN 25 AND 32));
--
