import psycopg2
from math import sqrt
import matplotlib.pyplot as plt

import paramiko
from sshtunnel import SSHTunnelForwarder


simulation_set_id = 1
index_of_active = 3

group_simulation = []
simuDataDict = {}
simulationResult = {}

#tunnel =  SSHTunnelForwarder(("pil-15.univlehavre.lan",22),ssh_username="litistech",ssh_private_key="/home/julienlitis/.ssh/id_rsa",ssh_password="gama17$!",remote_bind_address=('localhost', 5432),local_bind_address=('localhost',6543),)

#tunnel.start()

con = psycopg2.connect("dbname=gamaexp user=gama password=gama")

cur = con.cursor()


#We need to get all the simulation id from the set we study, group by their id_conf to do stat, and we also get the activation step now
cur.execute("SELECT p.id_simulation,id_configuration,s.activation_step FROM partofaset p INNER JOIN simulation s ON p.id_simulation = s.id_simulation WHERE id_simulationset = %s AND activation_step is not null;",(simulation_set_id,))

#Sorting by id conf, if the key (id_conf) doesn't exist, create it
for var in cur:
    if var[1] in simuDataDict:
        simuDataDict[var[1]].append((var[0],var[2]))
        simulationResult[var[1]] = ()
    else:
        simuDataDict[var[1]] = [(var[0],var[2])]

#Here we start analysing
for key in simuDataDict.keys():
    cpt = 0
    #Stockage variable
    averageValue = [0.0,0.0,0.0,0]
    averageNumberValue = [0,0,0.0]
    barycenterInfo = {}

    #For each simulation
    for tuple in simuDataDict[key]:
        averageNumber = {}
        averageNumberNervouss = {}
        nervousnessField = {}

        #For each agent we get his sim_id, aka the id gama give inside the simulation
        cur.execute("SELECT sim_id,id_simulation FROM agent WHERE id_simulation = %s",(tuple[0],))
        agent_set = cur.fetchall()
        for agent in agent_set:
            agent = agent + (tuple[1],)
            lastValue = (0,0,0)
            #Now we get all the state the agent get in the simulation
            cur.execute("SELECT * from state WHERE sim_id = %s AND id_simulation = %s AND step >= %s - 200 ORDER BY step", agent)

            for state in cur:
                i = 0
                #state[4] is an array, were each element is a tuple representing the state of the agent at a t time of the sim
                for step in state[4]:
                    #We check if the agent is activated and if the moment of the state is beyond the activation point, aka the moment from the wich it is relevant to analyse
                    if state[1]+i >= tuple[1] and int(step[0]) == 1:
                        #If the key (the time) does'nt exist create it, else increase the people counter
                        if state[1]+i in averageNumber:
                            averageNumber[state[1]+i] = averageNumber[state[1]+i] + 1
                        else:
                            averageNumber[state[1] + i] = 1

                        #Remaking of the nervousness field
                        if not state[1]//1000 in nervousnessField:
                            nervousnessField[state[1] // 1000] = {}
                        #Add the agent nervousness value to the cell where he is located at the time
                        if not (int(step[1]), int(step[2])) in nervousnessField[state[1]//1000]:
                            nervousnessField[state[1] // 1000][(int(step[1]), int(step[2]))] = (step[3],1)
                        else:
                            temp = nervousnessField[state[1]//1000][(int(step[1]),int(step[2]))]
                            nervousnessField[state[1]//1000][(int(step[1]),int(step[2]))] = (temp[0] +step[3],temp[1]+1)

                        #If the agent has nervousness > 0.5, increase the nervous people number
                        if step[3] > 0.5:
                            if state[1] + i in averageNumberNervouss:
                                averageNumberNervouss[state[1] + i] = averageNumberNervouss[state[1] + i] + 1
                            else:
                                averageNumberNervouss[state[1] + i] = 1

                        if lastValue[0] != 0:
                            #Speed calculation
                            averageValue[0] = averageValue[0] +  (sqrt((step[1]-lastValue[1])**2 + (step[2]-lastValue[2])**2))*100
                            averageValue[1] = averageValue[1] + (step[1]-lastValue[1])*100
                        lastValue = (1, step[1], step[2])

                        averageValue[2] = averageValue[2] + step[3]
                        cpt += 1

                    i = i +1

        barycenterInfo[tuple[0]] = []
        #Calculation of the barycenter for each time
        for time in nervousnessField.values():
            weight = 0.0
            x = 0.0
            y = 0.0
            for cell in time.keys():
                info = time[cell]
                nerv = info[0]/info[1]
                x = x + cell[0]*nerv
                y = y + cell[1]*nerv
                weight = weight + nerv
            x = x/weight
            y = y/weight
            barycenterInfo[tuple[0]].append((x,y,sqrt((x-35)**2+(y-3.5)**2)))





        sum = 0
        cpt2 = 0
        #print(averageNumber)
        #Compute average number of people
        for step in averageNumber.keys():
            sum = sum + averageNumber[step]
            cpt2 = cpt2+1
        if cpt2 != 0:
            averageNumberValue[0] = averageNumberValue[0] + (sum/cpt2)

        sum = 0
        cpt2 = 0
        #print(averageNumber)
        #Compute average number of nervous people
        for step in averageNumberNervouss.keys():
            sum = sum + averageNumberNervouss[step]
            cpt2 = cpt2+1
        if cpt2 != 0:
            averageNumberValue[1] = averageNumberValue[1] + (sum/cpt2)


    cpt2 = 0
    barySum = []
    for i in range(len(barycenterInfo.values())):
        barySum.append([0, 0, 0])

    print(barycenterInfo.values())
    for simulation in barycenterInfo.values():
        i = 0
        for moment in simulation:
            print(moment)
            barySum[i][0] = barySum[i][0] + moment[0]
            barySum[i][1] = barySum[i][1] + moment[1]
            barySum[i][2] = barySum[i][2] + moment[2]
        i = i + 1

        cpt2 = cpt2 + 1

    for i in range(len(barySum)):
        for j in range(len(barySum[i])):
            barySum[i][j] = barySum[i][j]/cpt2


    averageNumberValue[2] = averageNumberValue[1]/ averageNumberValue[0]


    for i in range(len(averageValue)):
        averageValue[i] = averageValue[i]/cpt
    averageNumberValue[0] = averageNumberValue[0]/len(simuDataDict[key])
    simulationResult[key] = (averageValue,averageNumberValue,barySum)


final_values = {}

for id in simulationResult:
    print(id)
    cur.execute("SELECT id_parameterset,commentaire FROM  configuration WHERE id_configuration = %s;",(id,))

    for var in cur:
        if var[0] in final_values:
            final_values[var[0]].append((var[1], simulationResult[id]))
        else:
            final_values[var[0]] = [(var[1], simulationResult[id])]

print(final_values)

plt.figure(figsize=(12.75,7
                    ))

for typeOfSimulation in final_values:
    print("new")
    legend = []
    axis = []
    for lambdaValue in final_values[typeOfSimulation]:
        legend.append(lambdaValue[0])
        axis.append(lambdaValue[1][0][0])
    print(legend)
    print(axis)
    plt.plot(legend,axis)

plt.title("Titre",fontsize=26)
plt.xlabel("axe x",fontsize=17)
plt.ylabel("axe y",fontsize=17)

axes = plt.gca()
axes.xaxis.set_tick_params(labelsize = 14)
axes.yaxis.set_tick_params(labelsize = 14)

plt.savefig('/home/julienlitis/Images/' + "test"+ '.png', format='png',bbox_inches="tight", dpi=400, pad_inches=0.1)


