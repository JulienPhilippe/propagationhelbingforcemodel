/*CREATE FUNCTION analyse_simulation(int)
RETURNS VOID AS $$
BEGIN
	INSERT INTO tmptableanalyse (
	Select 
		id_simulation,max(id_configuration) as id_configuration,avg(nervousness) as nervouness,(sum(locationX*nervousness))/sum(nervousness) as baryX,(sum(locationY*nervousness))/sum(nervousness) as baryY,count(*)/(count(distinct time)*1.0) as nPeople, sum(isNervous::int)/(count(distinct time)*1.0) as nbNervousPeople 
	FROM 
	(	select *,(tb1.nervousness>=0.5) AS isNervous 
		FROM  
			(select 
				si.id_simulation,id_configuration,sim_id,step,activation_step,unnest(states[array_upper(states,1)][2:2]) as locationX,unnest(states[array_upper(states,1)][3:3]) as locationY,unnest(states[array_upper(states,1)][4:4]) as nervousness,unnest(states[array_upper(states,1)][5:5]) as directionx,unnest(states[array_upper(states,1)][6:6]) as directiony,(step-200+generate_series(1,array_upper(states,1))-1) AS time 
			FROM state st INNER JOIN simulation si ON st.id_simulation = si.id_simulation  WHERE si.id_simulation = $1 AND states[1][1] = 1 AND NOT activation_step IS NULL) 
		as tb1 WHERE time >= activation_step and time <= 20000 AND locationX > 0 AND locationX<37)
	as tb2 group by id_simulation);
END
$$
LANGUAGE 'plpgsql';
*/

--Derouler de tout les agent d'une sim activer dans l'espace :
drop function if exists derouler_agent_simulation(int);
CREATE FUNCTION derouler_agent_simulation(int)
RETURNS TABLE
(
	sim_id INT,
	timeStep INT,
	locationX FLOAT,
	locationY FLOAT,
	nervousness FLOAT,
	directionX FLOAT,
	directionY FLOAT,	
	isnervous BOOLEAN
) AS $$
BEGIN
	RETURN QUERY select tb1.sim_id,tb1.time,tb1.locationX,tb1.locationY,tb1.nervousness,tb1.directionX,tb1.directionY,(tb1.nervousness>=0.5) AS isNervous 
			FROM  
				(select 
					si.id_simulation,id_configuration,st.sim_id,step,activation_step,unnest(states[array_upper(states,1)][2:2]) as locationX,unnest(states[array_upper(states,1)][3:3]) as locationY,unnest(states[array_upper(states,1)][4:4]) as nervousness,unnest(states[array_upper(states,1)][5:5]) as directionx,unnest(states[array_upper(states,1)][6:6]) as directiony,(step-200+generate_series(1,array_upper(states,1))-1) AS time 
				FROM state st INNER JOIN simulation si ON st.id_simulation = si.id_simulation  WHERE si.id_simulation = $1 AND states[1][1] = 1 AND NOT activation_step IS NULL) 
			as tb1 WHERE tb1.time >= activation_step and time <= 20000 AND tb1.locationX > 0 AND tb1.locationX<37;
END
$$
LANGUAGE 'plpgsql';


--Regrouper par le temps
drop function if exists derouler_temps_simulation(int);
CREATE FUNCTION derouler_temps_simulation(int)
RETURNS TABLE
(
	timeStep INT,
	nbPeople INT,
	densite FLOAT,
	nbNervous INT,
	nervousness FLOAT,
	baryX FLOAT,
	baryY FLOAT,
	distancebarydoor FLOAT
) AS $$
BEGIN
	RETURN QUERY select tb1.timeStep,tb1.nbPeople,tb1.densite,tb1.nbNervous,tb1.nervousness,tb1.baryX,tb1.baryY, (|/ (((tb1.baryx - 35::double precision) ^ 2::double precision) + ((tb1.baryy - 3.5::double precision) ^ 2::double precision))) AS distancebarydoor
			FROM
			(select q.timeStep,count(*) FILTER (where locationX<37)::int  as nbPeople,(count(*)/185.0)::float as densite,sum(q.isnervous::int)::int as nbNervous,avg(q.nervousness)::float as nervousness,(sum(q.locationX*q.nervousness))/(sum(q.nervousness)+0000000000.1) as baryX,(sum(q.locationY*q.nervousness))/(sum(q.nervousness)+0000000000.1) as baryY
			from derouler_agent_simulation($1) q
			group by q.timeStep order by densite desc) as tb1;
END
$$
LANGUAGE 'plpgsql';


DROP FUNCTION IF EXISTS analyse_simulation(int); 
CREATE FUNCTION analyse_simulation(int)
RETURNS VOID AS $$
BEGIN
	INSERT INTO tmptableanalyse (id_simulation,id_configuration,nervousness,baryx,baryy,nbpeople,nbnervouspeople,densite,distancebarydoor)
	(
	Select 
		$1 as id_simulation,id_configuration,avg(tb2.nervousness) as nervouness,(avg(tb2.baryX)) as baryX,(avg(tb2.baryY)) as baryY,avg(tb2.nbPeople) as nPeople,avg(tb2.nbNervous) as nbNervousPeople,avg(tb2.densite) as densite,avg(tb2.distancebarydoor) as distancebarydoor
	FROM 
	(	
		select *
		from derouler_temps_simulation($1))
	as tb2 inner join simulation s on s.id_simulation=$1  group by id_simulation);
END
$$
LANGUAGE 'plpgsql';

DROP TABLE IF EXISTS tempory_analyse_temporel;
CREATE TABLE tempory_analyse_temporel(
	id_simulation INT,
	timestep INT,
	nbpeople INT,
	densite FLOAT,
	nbnervous INT,
	nervousness FLOAT,
	baryx FLOAT,
	baryy FLOAT,
	distancebarydoor FLOAT
		
);

DROP FUNCTION IF EXISTS analyse_temporel_simulation(int);
CREATE FUNCTION analyse_temporel_simulation(int)
RETURNS TABLE
(
	id_configuration INT,
	name TEXT,
	commentaire TEXT,
	timestep INT,
	nbpeople FLOAT,
	densite FLOAT,
	nbNervous FLOAT,
	nervousness FLOAT,
	baryX FLOAT,
	baryY FLOAT,
	distancebarydoor FLOAT
)
AS $$
DECLARE
	result record;
BEGIN
	delete from tempory_analyse_temporel ;
	FOR result IN select * from simulation s inner join partofaset p on s.id_simulation = p.id_simulation where isover and p.id_simulationset = $1 and (s.id_simulation >= 980 OR s.id_configuration BETWEEN 12 AND 14 OR s.id_configuration BETWEEN 20 AND 22 OR s.id_configuration BETWEEN 28 AND 30)	LOOP
		RAISE NOTICE 'Analysing simulation number : % %',result.id_simulation, $1;
		INSERT INTO tempory_analyse_temporel (select result.id_simulation,* from derouler_temps_simulation(result.id_simulation));
	END LOOP;
	RETURN QUERY select s.id_configuration,max(c.name) as name,max(c.commentaire) as commentaire,t.timestep,avg(t.nbpeople)::float as nbpeople, avg(t.densite)::float as densite, avg(t.nbnervous)::float as nbnervous, avg(t.nervousness)::float as nervousness, avg(t.baryx)::float as baryx, avg(t.baryy)::float as baryy, avg(t.distancebarydoor)::float as distancedoor from tempory_analyse_temporel t inner join simulation s on s.id_simulation = t.id_simulation inner join configuration c ON s.id_configuration = c.id_configuration group by  s.id_configuration,t.timestep order by s.id_configuration,t.timestep;

	delete from tempory_analyse_temporel ;
END
$$
LANGUAGE 'plpgsql';


DROP function if exists analyse_toute_simulation();
CREATE FUNCTION analyse_toute_simulation()
RETURNS VOID AS $$
DECLARE
	idResult simulation%ROWTYPE;
BEGIN
	FOR idResult IN select * from simulation where isover
	LOOP
		RAISE NOTICE '%', idResult.id_simulation;
		perform analyse_simulation(idResult.id_simulation);
	END LOOP; 

END
$$
LANGUAGE 'plpgsql';



--En cours :

--Genration field
DROP function if exists derouler_champ_simulation(int);
CREATE FUNCTION derouler_champ_simulation(int)
RETURNS TABLE
(
	timestep INT,
	locationx INT,
	locationy INT,
	nervousness FLOAT,
	nbpeople INT
) AS $$
BEGIN
 RETURN QUERY select d.timestep,(CAST(floor(d.locationx) as int)) AS locationx,(CAST(floor(d.locationy) as int)) AS locationy,avg(d.nervousness) as nervousness,(CAST(count(d.sim_id) as int)) as nbpeople from derouler_agent_simulation($1) d
group by d.timestep,floor(d.locationx),floor(d.locationy)
order by d.timestep,floor(d.locationx),floor(d.locationy);
END
$$
LANGUAGE 'plpgsql';

DROP function if exists derouler_champ_simulation_interval(int,int);
CREATE function derouler_champ_simulation_interval(int,int)
RETURNS TABLE
(
	interval_time INT,
	locationx INT,
	locationy INT,
	nervousness FLOAT,
	nbpeople numeric
) AS $$
BEGIN
	RETURN QUERY select d.timestep/$2*$2 AS interval_time,d.locationx,d.locationy,avg(d.nervousness) AS nervousness,avg(d.nbpeople) as nbpeople from derouler_champ_simulation($1) d group by d.timestep/$2*$2,d.locationx,d.locationy
order by interval_time,d.locationx,d.locationy;
END
$$
LANGUAGE 'plpgsql';



DROP function if exists derouler_champ_simulation_set(int,int);
CREATE function derouler_champ_simulation_set(int,int)
RETURNS TABLE
(
	id_simulation INT	,
	interval_time INT,
	locationx INT,
	locationy INT,
	nervousness FLOAT,
	nbpeople numeric
) AS $$
DECLARE
	idResult simulation%ROWTYPE;
BEGIN
	CREATE TABLE temps_derouler_champ_simulation_set(id_simulation INT,	
	interval_time INT,
	locationx INT,
	locationy INT,
	nervousness FLOAT,
	nbpeople numeric);
	FOR idResult IN select s.id_simulation from simulation s INNER JOIN partofaset p on s.id_simulation = p.id_simulation WHERE isover and id_simulationset = $1
	LOOP
		RAISE NOTICE '%', idResult.id_simulation;
		INSERT INTO temps_derouler_champ_simulation_set select idResult.id_simulation,* from derouler_champ_simulation_interval(idResult.id_simulation,$2);
	END LOOP; 
	RETURN QUERY select * from temps_derouler_champ_simulation_set;
	DROP TABLE if exists temps_derouler_champ_simulation_set;
END
$$
LANGUAGE 'plpgsql';

DROP function if exists derouler_champ_simulation_set_avg(int,int);
CREATE function derouler_champ_simulation_set_avg(int,int)
RETURNS TABLE
(
	name varchar(25),
	commentaire varchar(50),
	interval_time INT,
	locationx INT,
	locationy INT,
	nervousness FLOAT,
	nbpeople numeric
) AS $$
BEGIN
	RETURN QUERY select c.name,c.commentaire,d.interval_time,d.locationx,d.locationy,avg(d.nervousness) as nervousness,avg(d.nbpeople) as nbpeople from derouler_champ_simulation_set($1,$2) d INNER JOIN simulation s ON d.id_simulation= s.id_simulation INNER JOIN configuration c ON s.id_configuration = c.id_configuration 
group by c.name,c.commentaire,d.interval_time,d.locationx,d.locationy
order by c.name,c.commentaire,d.interval_time,d.locationx,d.locationy;
END
$$
LANGUAGE 'plpgsql';

--Generate coupe
DROP function if exists derouler_coupe_simulation(int);
CREATE FUNCTION derouler_coupe_simulation(int)
RETURNS TABLE
(
	timestep INT,
	locationx INT,
	nervousness FLOAT,
	nbpeople INT
) AS $$
BEGIN
 RETURN QUERY select d.timestep,(CAST(floor(d.locationx) as int)) AS locationx,avg(d.nervousness) as nervousness,(CAST(count(d.sim_id) as int)) as nbpeople from derouler_agent_simulation($1) d
group by d.timestep,floor(d.locationx)
order by d.timestep,floor(d.locationx);
END
$$
LANGUAGE 'plpgsql';

DROP function if exists derouler_coupe_simulation_interval(int,int);
CREATE function derouler_coupe_simulation_interval(int,int)
RETURNS TABLE
(
	interval_time INT,
	locationx INT,
	nervousness FLOAT,
	nbpeople numeric
) AS $$
BEGIN
	RETURN QUERY select d.timestep/$2*$2 AS interval_time,d.locationx,avg(d.nervousness) AS nervousness,avg(d.nbpeople) as nbpeople from derouler_coupe_simulation($1) d group by d.timestep/$2*$2,d.locationx
order by interval_time,d.locationx;
END
$$
LANGUAGE 'plpgsql';

DROP function if exists derouler_coupe_simulation_set(int,int);
CREATE function derouler_coupe_simulation_set(int,int)
RETURNS TABLE
(
	id_simulation INT	,
	interval_time INT,
	locationx INT,
	nervousness FLOAT,
	nbpeople numeric
) AS $$
DECLARE
	idResult simulation%ROWTYPE;
BEGIN
	CREATE TABLE temps_derouler_coupe_simulation_set(id_simulation INT,	
	interval_time INT,
	locationx INT,
	nervousness FLOAT,
	nbpeople numeric);
	FOR idResult IN select s.id_simulation from simulation s INNER JOIN partofaset p on s.id_simulation = p.id_simulation WHERE isover and id_simulationset = $1
	LOOP
		RAISE NOTICE '%', idResult.id_simulation;
		INSERT INTO temps_derouler_coupe_simulation_set select idResult.id_simulation,* from derouler_coupe_simulation_interval(idResult.id_simulation,$2);
	END LOOP; 
	RETURN QUERY select * from temps_derouler_coupe_simulation_set;
	DROP TABLE if exists temps_derouler_coupe_simulation_set;
END
$$
LANGUAGE 'plpgsql';


DROP function if exists derouler_coupe_simulation_set_avg(int,int);
CREATE function derouler_coupe_simulation_set_avg(int,int)
RETURNS TABLE
(
	name varchar(25),
	commentaire varchar(50),
	interval_time INT,
	locationx INT,
	nervousness FLOAT,
	nbpeople numeric
) AS $$
BEGIN
	RETURN QUERY select c.name,c.commentaire,d.interval_time,d.locationx,avg(d.nervousness) as nervousness,avg(d.nbpeople) as nbpeople from derouler_coupe_simulation_set($1,$2) d INNER JOIN simulation s ON d.id_simulation= s.id_simulation INNER JOIN configuration c ON s.id_configuration = c.id_configuration 
group by c.name,c.commentaire,d.interval_time,d.locationx
order by c.name,c.commentaire,d.interval_time,d.locationx;
END
$$
LANGUAGE 'plpgsql';

--Evacuation d'une simulation
DROP function if exists taux_evacuation_simulation_seconde(int);
CREATE function taux_evacuation_simulation_seconde(int)
RETURNS FLOAT AS $$
DECLARE
	endTime INT;
	endNbPeople INT;
BEGIN
	select timeStep,nbpeople INTO endTime,endNbPeople from derouler_temps_simulation($1) order by timeStep DESC LIMIT 1;
	RETURN (select count(*)-endNbPeople as totalAgent from agent where id_simulation = $1 group by id_simulation)/(select (endTime -activation_step)/100.0 from simulation where id_simulation = $1);
END
$$
LANGUAGE 'plpgsql';


DROP function if exists taux_evacuation_all_simulation_set_seconde(int);
CREATE function taux_evacuation_all_simulation_set_seconde(int)
RETURNS TABLE
(
	id_simulation INT,	
	evacuation FLOAT
)  AS $$
DECLARE
	idResult simulation%ROWTYPE;
BEGIN
	drop table if exists tmp_evacuation_set;
	CREATE TABLE tmp_evacuation_set
(
	id_simulation INT,	
	evacuation FLOAT
);
	FOR idResult IN select s.id_simulation from simulation s INNER JOIN partofaset p on s.id_simulation = p.id_simulation WHERE isover and id_simulationset = $1
	LOOP
		RAISE NOTICE '%', idResult.id_simulation;
		INSERT INTO tmp_evacuation_set select idResult.id_simulation,* from taux_evacuation_simulation_seconde(idResult.id_simulation);
		update tmptableanalyse set tauxevacuation = evacuation where tmptableanalyse.id_simulation = idResult.id_simulation;
	END LOOP; 
	return query select * from tmp_evacuation_set;
	drop table if exists tmp_evacuation_set;
END
$$
LANGUAGE 'plpgsql';

DROP function if exists taux_evacuation_set_seconde(int);
CREATE function taux_evacuation_set_seconde(int)
RETURNS TABLE
(
	id_configuration INT,
	name varchar(25),
	commentaire varchar(50),
	evacutation FLOAT
)  AS $$

BEGIN
	return query select c.id_configuration,c.name,c.commentaire,avg(evacuation) from taux_evacuation_all_simulation_set_seconde($1) t INNER JOIN simulation s ON t.id_simulation = s.id_simulation INNER JOIN configuration c ON s.id_configuration = c.id_configuration group by c.id_configuration,c.name,c.commentaire order by c.id_configuration;
END
$$
LANGUAGE 'plpgsql';



