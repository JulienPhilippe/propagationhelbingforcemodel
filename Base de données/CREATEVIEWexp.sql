DROP VIEW view_tmptableanalyseaverage;
CREATE VIEW view_tmptableanalyseaverage AS
SELECT 
    p.id_simulationset,
    t.id_configuration,
    avg(t.nervousness) AS nervousness,
    avg(t.nbpeople) AS nbpeople,
    avg(t.nbpeople/185.0) AS densite,
    avg(t.nbnervouspeople) AS nervouspeople,
    avg(t.baryx) AS baryx,
    avg(t.baryy) AS baryy,
    avg(|/ (((t.baryx - 35::double precision) ^ 2::double precision) + ((t.baryy - 3.5::double precision) ^ 2::double precision))) AS distancebarydoor,
    max(c.name::text) AS name,
    max(c.commentaire::text) AS poisson_lambda
   FROM tmptableanalyse t
     JOIN configuration c ON t.id_configuration = c.id_configuration
     JOIN partofaset p ON t.id_simulation = p.id_simulation
  GROUP BY p.id_simulationset,t.id_configuration
  ORDER BY p.id_simulationset,(max(c.name::text)), (max(c.commentaire::text));
GRANT ALL PRIVILEGES ON view_tmptableanalyseaverage TO gama;

