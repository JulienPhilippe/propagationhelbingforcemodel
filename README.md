# HelbingForceModel

Implementation of the Helbing Social force model with the **Gama Platform**

See [Helbing, D., Farkas, I. J., Molnar, P., & Vicsek, T. (2002). Simulation of pedestrian crowds in normal and evacuation situations. Pedestrian and evacuation dynamics, 21(2), 21-58.](https://pdfs.semanticscholar.org/7f6a/ee02aecbce0fdd34fb88b6271061a1e02584.pdf)

How to launch a set of simulation :

1- Setup the database (see the read me in the database folder)

2- Create the config file
	a-Create XML file of the following format :

<Experiment_plan parallel="true"> <!--Wrap the all block, the parameter allows all the simulation IN THE BLOCk to run in parralel-->
 <Simulation id="1" sourcePath="path from this file to the gama experiment file" finalStep="Total number of step you want (stopping in the simulation code don't work in headless mode"  experiment="Name of the experiment in the gama experiment file">
  <Parameters>
    <Parameter name="Configuration id" type="INT" value="id of the configuration in the table configuration in the DB" />
    <Parameter name="Simulation set id" type="INT" value="id of the simulation set, permit us to regroup simulation by different set of experiment to be analyse separetly" />
  </Parameters>
 </Simulation>
 
<!-- exemple -->
<Simulation id="2" sourcePath="../../gama_workspace/HelbingForceModel/models/Experiment/InteractionEscapeCrowdExperiment.gaml" finalStep="20001"  experiment="MAIN_EXPERIMENT_corridorExit">
  <Parameters>
    <Parameter name="Configuration id" type="INT" value="80" />
	<Parameter name="Simulation set id" type="INT" value="7" />
  </Parameters>
 </Simulation>
	...

 </Simulation>

</Experiment_plan>

	b- in this files set a number of simulation depending on your computing power, if you have a lot of simulations make several file

	c- to execute the file, create a shell script (give it the right to be execute)

	#!/bin/sh
	java -cp $GAMA_CLASSPATH -Xms"min RAM POWER"m -Xmx"max RAM POWER"m -Djava.awt.headless=true org.eclipse.core.launcher.Main -application  msi.gama.headless.id4 -hcp19 "path to your xml file" "path of the folder in wich, eventually, your simulation will save files"
	
	"exemple"
	java -cp $GAMA_CLASSPATH -Xms512m -Xmx20048m -Djava.awt.headless=true org.eclipse.core.launcher.Main -application  msi.gama.headless.id4 -hcp19 Epsilontest1.xml ../../ExperimentResult/Epsilontest/
	java -cp $GAMA_CLASSPATH -Xms512m -Xmx20048m -Djava.awt.headless=true org.eclipse.core.launcher.Main -application  msi.gama.headless.id4 -hcp19 Epsilontest2.xml ../../ExperimentResult/Epsilontest/

	each line execute when the simulatins launch by the previous is over. You can setup the file to cover all the simulation, or even launch multiple time the same file to launch the same simulation

	d- execute the script and wait for the result

