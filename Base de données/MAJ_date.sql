alter table agent  add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table agentset add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table area add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table circularobstacle add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table configuration add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table includeagent add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table includeobstacle  add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table interactions add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table obstacle add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table parameterset add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table partofaset add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table simulation add insert_date timestamp DEFAULT CURRENT_TIMESTAMP;
alter table simulationset add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table state add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table tmptableanalyse add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;
alter table wall add insert_date timestamp  DEFAULT CURRENT_TIMESTAMP;

!--alter table agent  add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table agentset add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table area add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table circularobstacle add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table configuration add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table includeagent add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table includeobstacle  add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table interactions add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table obstacle add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table parameterset add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table partofaset add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table simulation add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table simulationset add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table state add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table tmptableanalyse add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;
!--alter table wall add last_update_date timestamp  DEFAULT CURRENT_TIMESTAMP;

CREATE FUNCTION update_date_agent RETURNS TRIGGER AS $$
BEGIN
	UPDATE agent SET last_update_date = now WHERE id_agent =NEW.id_agent;
	RETURN NEW;
END; $$
LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER update_date_agent_trigger
BEFORE UPDATE ON agent
FOR EACH ROW
EXECUTE PROCEDURE update_date_agent;


