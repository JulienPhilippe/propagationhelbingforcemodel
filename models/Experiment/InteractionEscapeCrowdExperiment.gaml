/**
* Name: InteractionEscapeCrowdExperiment
* Author: Julien PHILIPPE
* Description: 
*/

model InteractionEscapeCrowdExperiment

import "../Scheduler/InteractionScheduler.gaml"

experiment helbingPanicSimulation type: gui
{
	parameter 'Configuration id' var: id_configuration init:27 ;
	parameter 'Simulation set id' var: id_simulationset init:2 ;
	
	output
	{
		display SocialForceModel_display type:opengl
		{
			species interactionPeople;
			species wall;

           
			
		}
		
		display SocialForceModel_Field {
            species field aspect:aspectNervousness;
        }
        
        display SocialForceModel_FieldTotal {
            species field aspect:aspectNervousnessTotal;
        }
        
        display SocialForceModel_FieldTemporal {
            species field aspect:aspectNervousnessTemporal;
        }
        
		
		display SocialForceModel_NBinteractionPeople
		{
			chart "Number of interactionPeoples still inside " {
				data "nb_interactionPeople" value: nb_interactionPeople;
				
			}
					
		}
		
		monitor "Nb interactionPeople" value:nb_interactionPeople;
		monitor "Leaving time" value:lastCycle*deltaT;
		
	}

}

experiment MAIN_EXPERIMENT_corridorExit parent:helbingPanicSimulation
{
	parameter 'Space length' var: spaceLength init:60;
	parameter 'Space width' var: spaceWidth init:7;
	parameter 'Demonstration mode (not registering)' var:demonstrationMode init:true;

	
	output
	{
		
		display SocialForce_Passing
		{
			chart "BottleNeck Passing" {
				data "Nb people passing/seconde" value:peoplePass/(cycle+1)/deltaT;
			}
		}
		
		}
		
		
}

