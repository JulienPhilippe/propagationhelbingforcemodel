import psycopg2
import datetime
from math import sqrt
import matplotlib.pyplot as plt

con = psycopg2.connect("dbname=gamaexp user=gama password=gama")

cur = con.cursor()
date = datetime.datetime.now();
print("New analyse : " + str(datetime.datetime.now()) + "\n\n")

print("Nervousness analyse : " + str(datetime.datetime.now()) + "\n")

cur.execute("SELECT id_configuration,avg(nervousness) "
            "FROM (select id_simulation,id_configuration,sim_id,step,activation_step,unnest(states[array_upper(states,1)][2:2]) as locationX,"
            "unnest(states[array_upper(states,1)][3:3]) as locationY,"
            "unnest(states[array_upper(states,1)][4:4]) as nervousness,"
            "unnest(states[array_upper(states,1)][5:5]) as directionx,"
            "unnest(states[array_upper(states,1)][6:6]) as directiony,"
            " (step+generate_series(1,array_upper(states,1))-1)*10 AS time "
            "FROM state NATURAL JOIN simulation  WHERE states[1][1] = 1 AND NOT activation_step IS NULL) "
            "as tb1 "
            "WHERE time >= activation_step "
            "GROUP BY id_configuration ORDER BY id_configuration;",)

for elem in cur.fetchall():
    print(elem)

print("\nEnd nervousness analyse : " + str(datetime.datetime.now()) + " duration : " + str(datetime.datetime.now()-date) + "\n")

date = datetime.datetime.now()
print("Analysing number of people : " + str(datetime.datetime.now())+ "\n")

cur.execute("SELECT id_configuration,avg(nb_people) "
"FROM "
"(SELECT id_configuration,count(*) as nb_people,time "
            "FROM (select id_simulation,id_configuration,sim_id,step,activation_step,unnest(states[array_upper(states,1)][2:2]) as locationX, "
            "unnest(states[array_upper(states,1)][3:3]) as locationY, "
            "unnest(states[array_upper(states,1)][4:4]) as nervousness, "
            "unnest(states[array_upper(states,1)][5:5]) as directionx, "
            "unnest(states[array_upper(states,1)][6:6]) as directiony, "
             "(step+generate_series(1,array_upper(states,1))-1)*10 AS time "
            "FROM state NATURAL JOIN simulation  WHERE states[1][1] = 1 AND NOT activation_step IS NULL) "
            "as tb1 "
            "WHERE time >= activation_step "
            "GROUP BY id_configuration,time ORDER BY id_configuration,time) as t1 "
"GROUP BY id_configuration;")

for elem in cur.fetchall():
    print(elem)

print("\nEnd number people analyse : " + str(datetime.datetime.now()) + " duration : " + str(datetime.datetime.now()-date) + "\n")

date = datetime.datetime.now()
print("Analysing number of nervous people : " + str(datetime.datetime.now())+ "\n")

cur.execute("SELECT id_configuration,avg(nb_people) "
"FROM "
"(SELECT id_configuration,count(*) as nb_people,time "
            "FROM (select id_simulation,id_configuration,sim_id,step,activation_step,unnest(states[array_upper(states,1)][2:2]) as locationX, "
            "unnest(states[array_upper(states,1)][3:3]) as locationY, "
            "unnest(states[array_upper(states,1)][4:4]) as nervousness, "
            "unnest(states[array_upper(states,1)][5:5]) as directionx, "
            "unnest(states[array_upper(states,1)][6:6]) as directiony, "
             "(step+generate_series(1,array_upper(states,1))-1)*10 AS time "
            "FROM state NATURAL JOIN simulation  WHERE states[1][1] = 1 AND NOT activation_step IS NULL) "
            "as tb1 "
            "WHERE time >= activation_step AND nervousness >= 0.5"
            "GROUP BY id_configuration,time ORDER BY id_configuration,time) as t1 "
"GROUP BY id_configuration;")

for elem in cur.fetchall():
    print(elem)

print("\nEnd of nervous people analyse : " + str(datetime.datetime.now()) + " duration : " + str(datetime.datetime.now()-date) + "\n")