-------------SIMULATION PARAMETER-------------- 
---Lower Threshold
INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Lower Threshold',FALSE,40,2,TRUE,0.1,60,7,2000,0.08,0.12,120000,240000,TRUE,0.1);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Lower Threshold',FALSE,40,2,TRUE,0.5,60,7,2000,0.08,0.12,120000,240000,TRUE,0.1);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Lower Threshold',FALSE,40,2,TRUE,0.9,60,7,2000,0.08,0.12,120000,240000,TRUE,0.1);

---Low Threshold
INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Low Threshold',FALSE,40,2,TRUE,0.1,60,7,2000,0.08,0.12,120000,240000,TRUE,0.3);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Low Threshold',FALSE,40,2,TRUE,0.5,60,7,2000,0.08,0.12,120000,240000,TRUE,0.3);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Low Threshold',FALSE,40,2,TRUE,0.9,60,7,2000,0.08,0.12,120000,240000,TRUE,0.3);

---Middle Threshold
INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Middle Threshold',FALSE,40,2,TRUE,0.1,60,7,2000,0.08,0.12,120000,240000,TRUE,0.5);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Middle Threshold',FALSE,40,2,TRUE,0.5,60,7,2000,0.08,0.12,120000,240000,TRUE,0.5);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Middle Threshold',FALSE,40,2,TRUE,0.9,60,7,2000,0.08,0.12,120000,240000,TRUE,0.5);

---High Threshold
INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'High Threshold',FALSE,40,2,TRUE,0.1,60,7,2000,0.08,0.12,120000,240000,TRUE,0.7);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'High Threshold',FALSE,40,2,TRUE,0.5,60,7,2000,0.08,0.12,120000,240000,TRUE,0.7);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'High Threshold',FALSE,40,2,TRUE,0.9,60,7,2000,0.08,0.12,120000,240000,TRUE,0.7);

---Higher Threshold
INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Higher Threshold',FALSE,40,2,TRUE,0.1,60,7,2000,0.08,0.12,120000,240000,TRUE,0.9);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Higher Threshold',FALSE,40,2,TRUE,0.5,60,7,2000,0.08,0.12,120000,240000,TRUE,0.9);

INSERT INTO parameterset
(fluctuationType,deltaT,relaxationTime,isRespawn,isFluctuation,pedestrianSpeed,pedestrianMaxSpeed,pedestrianMaxSize,pedestrianMinSize,simulationDuration,temporalFieldIntervalLength,interactionType,is360,interactionAngle,interactionRange,isNervousnessTransmition,interactionParameter,spaceLength,spaceWidth,socialForceStrength,socialForceThreshold,socialForceVision,bodyContactStrength,bodyFrictionStrength,isNervousness,threshold)
VALUES ('Speed',0.01,0.2,FALSE,TRUE,1.34,10,0.35,0.25,20000,1000,'Higher Threshold',FALSE,40,2,TRUE,0.9,60,7,2000,0.08,0.12,120000,240000,TRUE,0.9);


-------CONFIG----------
----Lambda 3
--Low interaction
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter Lower Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.1));   

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter Low Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.3));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter Middle Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.5));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter High Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.7));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter Higher Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.9));  

--Medium interaction
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter Lower Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.1));   

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter Low Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.3));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter Middle Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.5));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter High Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.7));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter Higher Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.9));

--High interaction
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter Lower Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.1));   

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter Low Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.3));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter Middle Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.5));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter High Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.7));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter Higher Th','lambda : 3','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.9));

----Lambda 4
--Low interaction
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter Lower Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.1));   

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter Low Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.3));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter Middle Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.5));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter High Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.7));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter Higher Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.9));  

--Medium interaction
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter Lower Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.1));   

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter Low Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.3));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter Middle Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.5));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter High Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.7));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter Higher Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.9));

--High interaction
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter Lower Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.1));   

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter Low Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.3));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter Middle Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.5));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter High Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.7));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter Higher Th','lambda : 4','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.9));

----Lambda 5
--Low interaction
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter Lower Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.1));   

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter Low Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.3));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter Middle Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.5));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter High Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.7));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Low inter Higher Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.1 AND threshold= 0.9));  

--Medium interaction
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter Lower Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.1));   

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter Low Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.3));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter Middle Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.5));  

VALUES ('Medium inter High Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.7));  
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('Medium inter Higher Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.5 AND threshold= 0.9));

--High interaction
INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter Lower Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.1));   

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter Low Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.3));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter Middle Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.5));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter High Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.7));  

INSERT INTO configuration (name,commentaire,typeConfig,id_parameterSet)
VALUES ('High inter Higher Th','lambda : 5','Bottleneck',(Select id_parameterset from parameterset where interactionParameter= 0.9 AND threshold= 0.9));

--Include Obstacle
INSERT INTO includeobstacle (id_obstacle,id_configuration)
(SELECT id_obstacle,id_configuration FROM obstacle,configuration WHERE name like '%Th%');

--INCLUDE AGENTSET
INSERT INTO includeagent (id_configuration,id_agentSet,id_area,listOrder)
(SELECT id_configuration, id_agentset,id_area,id_area FROM configuration,agentset, area WHERE id_agentset = 2 AND name like '%Th%' AND commentaire like '%3%');

INSERT INTO includeagent (id_configuration,id_agentSet,id_area,listOrder)
(SELECT id_configuration, id_agentset,id_area,id_area FROM configuration,agentset, area WHERE id_agentset = 3 AND name like '%Th%' AND commentaire like '%4%');

INSERT INTO includeagent (id_configuration,id_agentSet,id_area,listOrder)
(SELECT id_configuration, id_agentset,id_area,id_area FROM configuration,agentset, area WHERE id_agentset = 4 AND name like '%Th%' AND commentaire like '%5%');




